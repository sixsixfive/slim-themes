#!/bin/sh
cd `dirname $0`
command -v convert >/dev/null 2>&1 || { echo >&2 "I require imagemagick but it's not installed.  Aborting."; exit 1; }
command -v sed >/dev/null 2>&1 || { echo >&2 "I require sed but it's not installed.  Aborting."; exit 1; }
read -p "Please enter your new color in HEX(eg. #ffffff for white) " NEWCOLOR
echo "This may take a while"
#change color in all textfiles
sed -i 's/#a5e12d/'$NEWCOLOR'/g' slim.theme
#change color in all images
convert -define png:format=png32 panel.png -fill "$NEWCOLOR" -opaque "#a5e12d" panel.png
echo "done"
